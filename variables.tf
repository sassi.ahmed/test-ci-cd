variable "ami" {
  type        = string
  description = "REHL AMI ID in ca-central-1 Region"
  default     = "ami-0d270005f18b0539a"
}

variable "instance_type" {
  type        = string
  description = "Instance type"
  default     = "t2.micro"
}

variable "name_tag" {
  type        = string
  description = "Name of the EC2 instance"
  default     = "My EC2 Instance"
}


variable "key_pair_name" {
  type    = string
  default = "2024key"
}
